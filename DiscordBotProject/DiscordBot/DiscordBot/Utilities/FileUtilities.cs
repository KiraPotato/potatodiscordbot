﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */
using System.Linq;
using System.IO;
using Newtonsoft.Json;

using DSharpPlus.Entities;

namespace DiscordBot.Utilities
{
  public static class CommandUtilities
  {
    public static string CombineString(string seperator, params string[] stringArray)
      => stringArray.Aggregate((phrase, word) => $"{phrase}{seperator}{word}");
  }

  public static class FileUtilities
  {
    public static bool LoadFile<T>(string directoryPath, string fileName, out T loadedObject)
    {
      loadedObject = default;

      if (string.IsNullOrEmpty(fileName))
        return false;

      makeDirectory(directoryPath);

      string filePath = directoryPath + fileName;
      bool exists = File.Exists(filePath);
      if (File.Exists(filePath) == false)
        return false;

      try
      {
        string fileContent = File.ReadAllText(filePath);

        if (string.IsNullOrEmpty(fileContent))
          return false;

        loadedObject = JsonConvert.DeserializeObject<T>(fileContent);

        return loadedObject != null;
      }
      catch (System.Exception)
      {
        return false;
      }
    }

    public static bool SaveFile<T>(string directoryPath, string fileName, T objectToSave, bool overrideFile = true)
    {
      if (objectToSave == null || string.IsNullOrEmpty(fileName))
        return false;

      makeDirectory(directoryPath);

      var filePath = directoryPath + fileName;
      if (File.Exists(filePath) && overrideFile == false)
        return false;

      var fileContent = JsonConvert.SerializeObject(objectToSave, Formatting.Indented);
      File.WriteAllText(filePath, fileContent);
      return true;
    }

    public static string FetchGuildFolder(DiscordGuild guild)
    {
      if (guild == null)
        return string.Empty;

      return FetchGuildFolder(guild.Id);
    }

    public static string FetchGuildFolder(ulong guildId)
    {
      var dirPath = Constants.GUILD_DATA_FOLDER + guildId.ToString() + "/";
      Directory.CreateDirectory(dirPath);
      return dirPath;
    }

    private static void makeDirectory(string directoryPath)
    {
      if (string.IsNullOrEmpty(directoryPath))
        return;

      Directory.CreateDirectory(directoryPath);
    }
  }
}