﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using TwitchLib.Api;
using TwitchLib.Api.Services;
using TwitchLib.Api.Services.Events;
using TwitchLib.Api.Services.Events.LiveStreamMonitor;

using DiscordBot.Config;

namespace DiscordBot.Twitch
{
  using FollowMap = Dictionary<string, List<TwitchStreamLink>>;
  using ChannelList = List<string>;

  public class TwitchLive
  {
    public static bool RecalculateList
    {
      get => _recalculateList;
      set
      {
        if (value == true)
          _recalculateList = true;
      }
    }

    private static bool _recalculateList;
    private static TwitchLive _instance;

    private LiveStreamMonitorService _monitor;
    private TwitchAPI _api;
    private FollowMap _followMap;
    private ChannelList _channelList;

    public TwitchLive()
    {
      _recalculateList = true;
      _followMap = new FollowMap();
      _channelList = new ChannelList();
    }

    public async static Task Start()
    {
      if (_instance == null)
        _instance = new TwitchLive();
      else
        return;

      await _instance.configureLiveMonitorAsync();
    }

    private async Task configureLiveMonitorAsync()
    {
      createApi();
      createMonitor();

      _monitor.Start();

      await Task.Delay(-1);
    }

    private void createMonitor()
    {
      _monitor = new LiveStreamMonitorService(_api, 60);

      _monitor.OnStreamOnline += onStreamOnline;
      _monitor.OnStreamOffline += onStreamOffline;
      _monitor.OnStreamUpdate += onStreamUpdate;
      _monitor.OnServiceStarted += onServiceStarted;
      _monitor.OnChannelsSet += onChannelSet;
    }

    private void createApi()
    {
      _api = new TwitchAPI();

      var config = MainConfig.TwitchConfig;

      _api.Settings.ClientId = config.ClientId;
      _api.Settings.AccessToken = config.Token;
    }

    private void updateList()
    {
      if (RecalculateList == false || _monitor == null)
        return;

      if (_channelList == null)
        _channelList = new ChannelList();


      GuildConfigs.FillTwitchLinks(ref _followMap);

      _channelList.Clear();
      _channelList.AddRange(_followMap.Keys);
      _monitor.SetChannelsById(_channelList);
    }

    private void onServiceStarted(object sender, OnServiceStartedArgs e)
    {
      Console.WriteLine("onServiceStarted");
      updateList();
    }

    private void onChannelSet(object sender, OnChannelsSetArgs e)
    {
      Console.WriteLine("onChannelSet");
    }

    private void onStreamUpdate(object sender, OnStreamUpdateArgs e)
    {
      Console.WriteLine("onStreamUpdate");
    }

    private void onStreamOffline(object sender, OnStreamOfflineArgs e)
    {
      Console.WriteLine("onStreamOffline");
    }

    private void onStreamOnline(object sender, OnStreamOnlineArgs e)
    {
      Console.WriteLine("onStreamOnline");
    }
  }
}