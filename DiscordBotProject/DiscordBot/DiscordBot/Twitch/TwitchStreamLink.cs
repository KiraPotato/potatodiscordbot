﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */
using System;
using System.Collections.Generic;
using DSharpPlus.Entities;
using DiscordBot.Config.GuildConfigs.Shared;

namespace DiscordBot
{
  public struct TwitchStreamLink
  {
    public string StreamName { get; private set; }
    public ulong GuildId { get; private set; }
    public ulong ChannelId { get; private set; }

    public TwitchStreamLink(string streamName, ulong guildId, ulong channelId)
    {
      StreamName = streamName;
      GuildId = guildId;
      ChannelId = channelId;
    }

  }
}
