﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */
using Newtonsoft.Json;

namespace DiscordBot.Config.Main
{
  [JsonObject(MemberSerialization.OptIn)]
  public struct TwitchConfigJson
  {
    private const string DEFAULT_CLIENT_ID = "Enter your client id here";
    private const string DEFAULT_CLIENT_TOKEN = "Enter your client token here";

    public bool IsDataValid
    {
      get
      {
        if (string.IsNullOrEmpty(ClientId) || ClientId == DEFAULT_CLIENT_ID)
          return false;

        if (string.IsNullOrEmpty(Token) || Token == DEFAULT_CLIENT_TOKEN)
          return false;

        return true;
      }
    }

    public static TwitchConfigJson NewDefault
    {
      get
      {
        return new TwitchConfigJson
        {
          ClientId = DEFAULT_CLIENT_ID,
          Token = DEFAULT_CLIENT_TOKEN
        };
      }
    }

    [JsonProperty("ClientId")]
    public string ClientId { get; private set; }

    [JsonProperty("Token")]
    public string Token { get; private set; }
  }
}
