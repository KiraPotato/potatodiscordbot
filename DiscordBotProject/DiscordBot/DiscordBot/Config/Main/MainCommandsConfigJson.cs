﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */
using Newtonsoft.Json;

namespace DiscordBot.Config.Main
{
  [JsonObject(MemberSerialization.OptIn)]
  public struct MainCommandsConfigJson
  {
    private const string DEFAULT_PREFIX = "!";

    public bool IsValid
      => string.IsNullOrEmpty(CommandPrefix) == false;

    [JsonProperty("commandPrefix")]
    public string CommandPrefix { get; private set; }

    [JsonProperty("caseSensitive")]
    public bool CaseSensitive { get; private set; }

    [JsonProperty("enableDefaultHelp")]
    public bool EnableDefaultHelp { get; private set; }

    [JsonProperty("ignoreExtraArguments")]
    public bool IgnoreExtraArguments { get; private set; }

    [JsonProperty("enableMentionPrefix")]
    public bool EnableMentionPrefix { get; private set; }

    public bool EnableDms => false; // This should not be changed until bot can actually support Dms

    public static MainCommandsConfigJson NewDefault
    {
      get
      {
        return new MainCommandsConfigJson
        {
          CommandPrefix = DEFAULT_PREFIX,
          CaseSensitive = true,
          EnableDefaultHelp = true,
          IgnoreExtraArguments = true,
          EnableMentionPrefix = true
        };
      }
    }
  }
}
