﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */
using System;
using System.IO;
using Newtonsoft.Json;

using static DiscordBot.Constants;
using static DiscordBot.Utilities.FileUtilities;

namespace DiscordBot.Config.Main
{
  [JsonObject(MemberSerialization.OptIn)]
  public class MainConfigJson
  {
    private const string FILE_NAME = "/MainSettings.json";
    private const string MAIN_COFIG_CREATED_FORMAT = "Main config file {0} was created at {1}, Please edit it and then restart the bot.";
    private const string MAIN_CONFIG_LOADED_FORMAT = "MainConfigsLoaded {0}";

    public bool IsValid
      => DiscordConfig.IsDataValid && CommandsConfig.IsValid;

    [JsonProperty("discordSettings")]
    public DiscordConfigJson DiscordConfig { get; private set; }

    [JsonProperty("commandsConfig")]
    public MainCommandsConfigJson CommandsConfig { get; private set; }

    [JsonProperty("twitchConfig")]
    public TwitchConfigJson TwitchConfig { get; private set; }

    public static MainConfigJson Load()
    {
      var dir = Directory.CreateDirectory(MAIN_CONFIG_DIR);

      if (LoadFile<MainConfigJson>(MAIN_CONFIG_DIR, FILE_NAME, out var loadedMainConfig))
      {
        Console.WriteLine(MAIN_CONFIG_LOADED_FORMAT, dir.FullName + FILE_NAME);
        loadedMainConfig.Save();
        return loadedMainConfig;
      }

      var newConfig = fetchNewDefault();
      newConfig.Save();

      Console.WriteLine(string.Format(MAIN_COFIG_CREATED_FORMAT, FILE_NAME, dir.FullName));
      Environment.Exit(-1);

      return newConfig;
    }

    public bool Save() => SaveFile(MAIN_CONFIG_DIR, FILE_NAME, this);

    private static MainConfigJson fetchNewDefault()
    {
      return new MainConfigJson
      {
        DiscordConfig = DiscordConfigJson.NewDefault,
        CommandsConfig = MainCommandsConfigJson.NewDefault,
        TwitchConfig = TwitchConfigJson.NewDefault
      };
    }
  }
}
