﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */
using Newtonsoft.Json;

using DSharpPlus;

namespace DiscordBot.Config.Main
{
  [JsonObject(MemberSerialization.OptIn)]
  public struct DiscordConfigJson
  {
    private const string DEFAULT_TOKEN_VALUE = "Enter Token Here";

    public bool IsDataValid
    {
      get
      {
        if (string.IsNullOrEmpty(Token) || Token == DEFAULT_TOKEN_VALUE)
          return false;

        return true;
      }
    }

    public static DiscordConfigJson NewDefault
    {
      get
      {
        return new DiscordConfigJson
        {
          Token = DEFAULT_TOKEN_VALUE,
          AutoReconnect = true,
          TokenType = TokenType.Bot,
          LogLevel = LogLevel.Debug
        };
      }
    }



    [JsonProperty("token")]
    public string Token { get; private set; }

    [JsonProperty("autoReconnect")]
    public bool AutoReconnect { get; private set; }

    [JsonProperty("tokenType")]
    public TokenType TokenType { get; private set; }

    [JsonProperty("logLevel")]
    public LogLevel LogLevel { get; private set; }
  }
}
