﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */
using Newtonsoft.Json;

namespace DiscordBot.Config.GuildConfigs.Shared
{
  [JsonObject(MemberSerialization.OptIn)]
  public abstract class BaseCommandConfig : BaseConfig
  {
    [JsonProperty("enabled")]
    public bool IsEnabled
    {
      get => _isEnabled;
      set
      {
        IsDirty = true;
        _isEnabled = value;
      }
    }

    private bool _isEnabled;

    public BaseCommandConfig(bool startEnabled) : base()
    {
      IsEnabled = startEnabled;
    }
  }
}
