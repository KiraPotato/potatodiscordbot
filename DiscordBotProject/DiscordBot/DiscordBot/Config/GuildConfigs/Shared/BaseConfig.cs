﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */
using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

using DSharpPlus.Entities;

using static DiscordBot.Utilities.FileUtilities;

namespace DiscordBot.Config.GuildConfigs.Shared
{
  [JsonObject(MemberSerialization.OptIn)]
  public abstract class BaseConfig
  {
    public abstract string Name { get; }
    public bool IsDirty { get; protected set; }

    protected abstract string _fileName { get; }

    [JsonProperty("editWhitelist")]
    private List<ulong> _editWhitelist = new List<ulong>();

    [JsonProperty("useWhitelist")]
    private List<ulong> _useWhitelist = new List<ulong>();
    private bool _allCanEdit => _editWhitelist == null || _editWhitelist.Count == 0;
    private bool _allCanUse => _useWhitelist == null || _useWhitelist.Count == 0;

    public BaseConfig()
    {
      IsDirty = true;
    }

    public void ToggleEditAllowance(bool isAllowed, params DiscordRole[] roles)
    {
      foreach (var role in roles)
      {
        toggleWhitelist(isAllowed, role, ref _editWhitelist);

        //if (isAllowed)
        //  toggleWhitelist(true, role, ref _useWhitelist);
      }
    }

    public void ToggleUseAllowance(bool isAllowed, params DiscordRole[] roles)
    {
      foreach (var role in roles)
      {
        toggleWhitelist(isAllowed, role, ref _useWhitelist);

        //if (isAllowed == false)
        //  toggleWhitelist(false, role, ref _editWhitelist);
      }
    }

    private void toggleWhitelist(bool isAllowed, DiscordRole role, ref List<ulong> whitelist)
    {
      if (role == null)
        return;

      if (whitelist == null)
        whitelist = new List<ulong>();

      if (isAllowed && whitelist.Contains(role.Id) == false)
      {
        whitelist.Add(role.Id);
        IsDirty = true;
      }
      else if (whitelist.Contains(role.Id))
      {
        whitelist.Remove(role.Id);
        IsDirty = true;
      }
    }


    public bool IsAllowedToEdit(DiscordMember user)
      => isAllowed(_allCanEdit, _editWhitelist, user);

    public bool IsAllowedToUse(DiscordMember user)
      => isAllowed(_allCanUse, _useWhitelist, user);

    private bool isAllowed(bool allAllowed, List<ulong> whitelist, DiscordMember user)
    {
      if (whitelist == null || allAllowed)
        return true;

      if (user == null)
        return false;

      if (user.IsOwner)
        return true;

      foreach (var role in user.Roles)
      {
        if (whitelist.Contains(role.Id))
          return true;
      }

      return false;
    }

    public static bool Load<T>(DiscordGuild guild, string fileName, out T loadedFile)
      where T : BaseConfig, new()
    {
      if (guild == null)
      {
        loadedFile = null;
        return false;
      }

      return Load(guild.Id, fileName, out loadedFile);
    }

    public static bool Load<T>(ulong guildId, string fileName, out T loadedFile)
      where T : BaseConfig, new()
    {
      if (string.IsNullOrEmpty(fileName))
      {
        loadedFile = null;
        return false;
      }

      var dirPath = FetchGuildFolder(guildId);
      var dir = Directory.CreateDirectory(dirPath);

      if (LoadFile(dirPath, fileName, out loadedFile))
      {
        Console.WriteLine("Loaded file {0}{1}", dir.FullName, fileName);
        return true;
      }

      loadedFile = new T();

      if (loadedFile.Save(guildId))
        Console.WriteLine("Created file {0}{1}", dir.FullName, fileName);

      return true;
    }

    public virtual bool Save(DiscordGuild guild)
    {
      IsDirty = !SaveFile(FetchGuildFolder(guild), _fileName, this);
      return !IsDirty;
    }

    public virtual bool Save(ulong guildId)
    {
      IsDirty = !SaveFile(FetchGuildFolder(guildId), _fileName, this);
      return !IsDirty;
    }
  }
}
