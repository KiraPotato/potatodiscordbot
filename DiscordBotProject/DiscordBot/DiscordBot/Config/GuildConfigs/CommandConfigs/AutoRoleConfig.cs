﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */
using System.Threading.Tasks;
using Newtonsoft.Json;

using DSharpPlus.EventArgs;

using DiscordBot.Config.GuildConfigs.Shared;

namespace DiscordBot.Config.GuildConfigs.CommandConfigs
{

  [JsonObject(MemberSerialization.OptIn)]
  public class AutoRoleConfig : BaseCommandConfig
  {
    public const string COMMAND_NAME = "AutoRole";
    public const string FILE_NAME = "AutoRoleConfig.json";

    public AutoRoleConfig() : base(true) { }

    [JsonProperty("RoleId")]
    public ulong RoleId { get; private set; }

    public override string Name => COMMAND_NAME;
    protected override string _fileName => FILE_NAME;

    public void SetRoleId(ulong newRoleId)
    {
      RoleId = newRoleId;
      IsDirty = true;
    }

    public async Task AssignRole(GuildMemberAddEventArgs e)
    {
      if (IsEnabled == false)
        return;

      if (e == null)
        return;

      var role = e.Guild.GetRole(RoleId);

      if (role == null)
        return;

      await e.Member.GrantRoleAsync(role, Constants.COMMAND_AUTOROLE_NAME);
    }
  }
}
