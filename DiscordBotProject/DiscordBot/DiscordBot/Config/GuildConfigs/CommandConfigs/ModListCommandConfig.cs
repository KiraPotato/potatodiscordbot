﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */
using System.Collections.Generic;
using Newtonsoft.Json;

using DSharpPlus.Entities;

using DiscordBot.Config.GuildConfigs.Shared;

namespace DiscordBot.Config.GuildConfigs.CommandConfigs
{
  [JsonObject(MemberSerialization.OptIn)]
  public class ModListCommandConfig : BaseCommandConfig
  {
    public const string COMMAND_NAME = "Mod List";
    public const string FILE_NAME = "ModListCommandConfig.json";

    public IReadOnlyCollection<string> AvailableNames
    {
      get
      {
        generateMap();

        return _entriesMap.Keys;
      }
    }
    public override string Name => COMMAND_NAME;
    protected override string _fileName => FILE_NAME;

    [JsonProperty("Entries")]
    private List<Entry> _entries = new List<Entry>();

    private Dictionary<string, string> _entriesMap;

    public ModListCommandConfig() : base(true) { }

    public bool ContainsEntry(string entryName)
    {
      generateMap();

      var name = entryName.ToLower();
      return _entriesMap.ContainsKey(name);
    }

    public bool FetchEntryMessage(string entryName, out string message)
    {
      message = string.Empty;

      var name = entryName.ToLower();

      if (ContainsEntry(name) == false)
        return false;

      message = _entriesMap[name];
      return true;
    }

    public bool AddEntry(string entryName, string message)
    {
      if (string.IsNullOrEmpty(entryName))
        return false;


      generateMap();

      var name = entryName.ToLower();
      _entriesMap[name] = message;
      IsDirty = true;
      return true;
    }

    public bool RemoveEntry(string entryName)
    {
      if (string.IsNullOrEmpty(entryName))
        return false;

      generateMap();

      var name = entryName.ToLower();

      if (_entriesMap.ContainsKey(name) == false)
        return false;

      _entriesMap.Remove(name);
      IsDirty = true;
      return true;
    }

    public override bool Save(DiscordGuild guild)
    {
      generateMap();

      _entries.Clear();
      foreach (var item in _entriesMap)
        _entries.Add(new Entry(item.Key, item.Value));

      return base.Save(guild);
    }

    private void generateMap()
    {
      if (_entriesMap != null)
        return;

      _entriesMap = new Dictionary<string, string>();

      if (_entries == null)
        return;

      foreach (var entry in _entries)
      {
        if (string.IsNullOrEmpty(entry.GameName))
          continue;

        _entriesMap[entry.GameName] = entry.Message;
      }
    }

    [JsonObject(MemberSerialization.OptIn)]
    public struct Entry
    {
      [JsonProperty("gameName")]
      public string GameName { get; private set; }
      [JsonProperty("message")]
      public string Message { get; private set; }

      public Entry(string name, string message)
      {
        GameName = name;
        Message = message;
      }
    }
  }
}
