﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */
using System.Collections.Generic;
using Newtonsoft.Json;

using DiscordBot.Config.GuildConfigs.Shared;

namespace DiscordBot.Config.GuildConfigs.CommandConfigs
{
  [JsonObject(MemberSerialization.OptIn)]
  public class TwitchLiveConfig : BaseCommandConfig
  {
    public const string COMMAND_NAME = "TwitchLive";
    public const string FILE_NAME = "TwitchLiveConfig.json";

    public override string Name => COMMAND_NAME;
    public IReadOnlyCollection<string> FollowedChannels => _channelNames;

    [JsonProperty("TargetDiscordChannel")]
    public ulong TargetDiscordChannel { get; private set; }

    [JsonProperty("ListenToStreamers")]
    private List<string> _channelNames = new List<string>();

    protected override string _fileName => FILE_NAME;

    public TwitchLiveConfig() : base(false) { }

    public bool FollowChannels(params string[] channels)
    {
      if (_channelNames == null)
        _channelNames = new List<string>();

      if (channels.Length == 0)
        return false;

      bool newAdded = false;

      foreach (var name in channels)
      {
        if (_channelNames.Contains(name))
          continue;

        _channelNames.Add(name);
        newAdded = true;
      }

      if (newAdded)
        IsDirty = true;

      return newAdded;
    }

    public bool UnfollowChannels(params string[] channels)
    {
      if (_channelNames == null || _channelNames.Count == 0 || channels.Length == 0)
        return false;

      bool removed = false;

      foreach (var name in channels)
      {
        if (_channelNames.Contains(name) == false)
          continue;

        _channelNames.Remove(name);
        removed = true;
      }

      if (removed)
        IsDirty = true;

      return removed;
    }
  }
}
