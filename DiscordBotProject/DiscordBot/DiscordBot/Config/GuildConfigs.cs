﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using DSharpPlus;
using DSharpPlus.Entities;

using DiscordBot.Config.GuildConfigs.CommandConfigs;
using DiscordBot.Config.GuildConfigs.Shared;

namespace DiscordBot
{
  using FileNameMap = Dictionary<Type, string>;
  using SettingsMap = Dictionary<ulong, Dictionary<Type, BaseConfig>>;

  public static class GuildConfigs
  {
    private static FileNameMap _fileNameMap { get; } = new FileNameMap
    {
      { typeof(ModListCommandConfig), ModListCommandConfig.FILE_NAME },
      { typeof(AutoRoleConfig), AutoRoleConfig.FILE_NAME },
      { typeof(TwitchLiveConfig), TwitchLiveConfig.FILE_NAME }
    };

    private static SettingsMap _settingsMap = new SettingsMap();

    public static T FetchConfigs<T>(DiscordGuild guild) where T : BaseConfig
    {
      if (guild == null)
        return null;

      return FetchConfigs<T>(guild.Id);
    }

    public static T FetchConfigs<T>(ulong guildId) where T : BaseConfig
    {
      if (_settingsMap.ContainsKey(guildId) == false)
        LoadGuildConfigs(guildId);

      if (_settingsMap[guildId].ContainsKey(typeof(T)) == false)
        return null;

      return _settingsMap[guildId][typeof(T)] as T;
    }

    public static void LoadGuildConfigs(DiscordGuild guild)
    {
      if (guild == null)
        return;

      Console.WriteLine("Loading config for guild {0}:{1}", guild.Name, guild.Id);

      LoadGuildConfigs(guild.Id);
    }

    public static void LoadGuildConfigs(ulong guildId)
    {
      if (_settingsMap.ContainsKey(guildId) == false)
        _settingsMap[guildId] = new Dictionary<Type, BaseConfig>();

      var perGuildMap = _settingsMap[guildId];

      loadAndAdd<ModListCommandConfig>(guildId, perGuildMap);
      loadAndAdd<AutoRoleConfig>(guildId, perGuildMap);
      loadAndAdd<TwitchLiveConfig>(guildId, perGuildMap);
    }

    public static async Task FileSaveCycle(int msDelay, DiscordClient client)
    {
      msDelay = Math.Max(1, msDelay);

      while (true)
      {
        await Task.Delay(msDelay);

        Console.WriteLine("Saving config files...");
        foreach (var guild in client.Guilds.Values)
        {
          if (_settingsMap.ContainsKey(guild.Id) == false)
            continue;

          foreach (var item in _settingsMap[guild.Id].Values)
          {
            if (item.IsDirty)
              item.Save(guild);
          }
        }
        Console.WriteLine("Saving config files saved.");
      }
    }

    public static void FillTwitchLinks(ref Dictionary<string, List<TwitchStreamLink>> linkMap)
    {
      if (linkMap == null)
        linkMap = new Dictionary<string, List<TwitchStreamLink>>();
      else
        linkMap.Clear();

      if (_settingsMap == null)
        return;

      foreach (var item in _settingsMap)
      {
        var guildId = item.Key;

        var config = FetchConfigs<TwitchLiveConfig>(guildId);

        if (config == null)
          continue;

        foreach (var channel in config.FollowedChannels)
        {
          if (linkMap.ContainsKey(channel) == false)
            linkMap[channel] = new List<TwitchStreamLink>();

          linkMap[channel].Add(
            new TwitchStreamLink(
              channel, guildId, config.TargetDiscordChannel));
        }
      }
    }

    private static void loadAndAdd<T>(DiscordGuild guild, Dictionary<Type, BaseConfig> dictionary) 
      where T : BaseConfig, new()
    {
      if (guild == null)
        return;

      loadAndAdd<T>(guild.Id, dictionary);
    }

    private static void loadAndAdd<T>(ulong guildid, Dictionary<Type, BaseConfig> dictionary) 
      where T : BaseConfig, new()
    {
      var fileName = fetchFileName<T>();

      if (string.IsNullOrEmpty(fileName))
        return;

      if (BaseConfig.Load<T>(guildid, fileName, out var loadedFile) == false)
        return;

      var type = typeof(T);
      dictionary[type] = loadedFile;
    }

    private static string fetchFileName<T>() where T : BaseConfig
    {
      var type = typeof(T);

      if (_fileNameMap.ContainsKey(type) == false)
        return string.Empty;

      return _fileNameMap[type];
    }
  }
}
