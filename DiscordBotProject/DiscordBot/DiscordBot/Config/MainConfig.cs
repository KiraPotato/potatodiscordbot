﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */
using DSharpPlus;
using DSharpPlus.CommandsNext;

using DiscordBot.Config.Main;

namespace DiscordBot.Config
{
  public static class MainConfig
  {
    public static bool IsDataValid
    {
      get
      {
        if (_mainConfig == null)
          _mainConfig = MainConfigJson.Load();

        return _mainConfig.IsValid;
      }
    }

    public static TwitchConfigJson TwitchConfig
    {
      get
      {
        if (_mainConfig == null)
          _mainConfig = MainConfigJson.Load();

        return _mainConfig.TwitchConfig;
      }
    }

    public static DiscordConfiguration DiscordConfiguration
    {
      get
      {
        if (_mainConfig == null)
          _mainConfig = MainConfigJson.Load();

        if (_discordConfig == null)
          _discordConfig = new DiscordConfiguration
          {
            Token = _mainConfig.DiscordConfig.Token,
            AutoReconnect = _mainConfig.DiscordConfig.AutoReconnect,
            TokenType = _mainConfig.DiscordConfig.TokenType,
            LogLevel = _mainConfig.DiscordConfig.LogLevel,
            UseInternalLogHandler = true,
          };

        return _discordConfig;
      }
    }

    public static CommandsNextConfiguration CommandsNextConfiguration
    {
      get
      {
        if (_mainConfig == null)
          _mainConfig = MainConfigJson.Load();

        if (_commandsNextConfig == null)
          _commandsNextConfig = new CommandsNextConfiguration
          {
            StringPrefix = _mainConfig.CommandsConfig.CommandPrefix,
            CaseSensitive = _mainConfig.CommandsConfig.CaseSensitive,
            EnableDefaultHelp = _mainConfig.CommandsConfig.EnableDefaultHelp,
            IgnoreExtraArguments = _mainConfig.CommandsConfig.IgnoreExtraArguments,
            EnableMentionPrefix = _mainConfig.CommandsConfig.EnableMentionPrefix,
            EnableDms = _mainConfig.CommandsConfig.EnableDms
          };

        return _commandsNextConfig;
      }
    }

    private static MainConfigJson _mainConfig;
    private static DiscordConfiguration _discordConfig;
    private static CommandsNextConfiguration _commandsNextConfig;
  }
}
