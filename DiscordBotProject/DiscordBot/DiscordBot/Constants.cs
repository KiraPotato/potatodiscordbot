﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */
namespace DiscordBot
{
  public static class Constants
  {
    public const string MAIN_CONFIG_DIR = "Settings/";
    public const string GUILD_DATA_FOLDER = MAIN_CONFIG_DIR + "Guild Data/";

    public const string INVALID_ROLES = "```Please mention the roles in question```";
    public const string OPERATION_COMPLETE = "```Operation Complete```";
    public const string NOT_ALLOWED = "```You are not allowed to use this command!```";

    #region Twitch Live Commands
    public const string COMMAND_TWITCH_LIVE_ADD = "twitchLive_add";
    public const string COMMAND_TWITCH_LIVE_REMOVE = "twitchLive_remove";
    public const string COMMAND_TWITCH_LIVE_SET_CHANNEL = "twitchLive_setMessageChannel";
    public const string COMMAND_TWITCH_LIVE_EDIT_WHITELIST_CHANGE = "twitchLive_whitelist_edit_change";
    #endregion

    #region modlist commands
    public const string COMMAND_MODLIST_NAME = "modList";
    public const string COMMAND_MODLIST_DESCRIPTION = "Shows the modlist for the requested name.\nUse without anything to see the available game list";

    public const string COMMAND_MODLIST_ADD_NAME = "modList_add";
    public const string COMMAND_MODLIST_REMOVE_NAME = "modlist_remove";
    public const string COMMAND_MODLIST_USE_WHITELIST_CHANGE = "modlist_whitelist_use_change";
    public const string COMMAND_MODLIST_EDIT_WHITELIST_CHANGE = "modlist_whitelist_edit_change";

    public const string COMMAND_MODLIST_ADD_USE_WHITELIST = "";
    public const string COMMAND_MODLIST_ADD_EDIT_WHITELIST = "";

    public const int COMMAND_MODLIST_TIMEOUT = 60;

    public const string MESSAGE_MODLIST_DISABLED = "All " + COMMAND_MODLIST_NAME + " commands are disabled on this server";
    public const string MESSAGE_MODLIST_REQUESTING_REPLY_FORMAT = "Please enter the message to display using mod command with one of the following values:\n{0}";
    public const string MESSAGE_MODLIST_SUCCESS_REPLY_FORMAT = "Whenever one of the values ({0}) are used, the following message will be displayed";
    public const string MESSAGE_MODLIST_TIMEOUT = "Request timed out...";
    public const string MESSAGE_MODLIST_AVAILABLE_BLANK = "No available entries available for " + COMMAND_MODLIST_NAME;
    public const string MESSAGE_MODLIST_AVAILABLE_REPLY_FORMAT = "Available games to use with the " + COMMAND_MODLIST_NAME + ":\n{0}";
    public const string MESSAGE_MODLIST_REMOVE_SUCCESS_REPLY_FORMAT = "The following items were removed from the modlist:\n{0}";
    #endregion

    #region autorole command
    public const string COMMAND_AUTOROLE_NAME = "autoRole_set";
    public const string COMMAND_AUTOROLE_DISABLE_NAME = "autoRole_disable";
    public const string COMMAND_AUTOROLE_RETRO_ADD_NAME = "autoRole_retroactive";
    public const string COMMAND_AUTOROLE_REMOVE_ROLE_FROM_ALL_NAME = "autoRole_remove";

    public const string MESSAGE_AUTOROLE_DISABLED_CONFIRMATION = "Auto role assignment was disabled";
    public const string MESSAGE_AUTOROLE_INCORRECT_ROLES_MENTIONED = "Please mention 1 role";
    public const string MESSAGE_AUTOROLE_ASSIGN_CONFIRMATION_FORMAT = "Role {0}:{1} will be automatically assigned to new members";
    public const string MESSAGE_AUTOROLE_ASSIGN_ROLE_FIRST = "Please assign role using " + COMMAND_AUTOROLE_NAME + " before trying to retroactively set it to all users";
    public const string MESSAGE_AUTOROLE_ASSIGN_RETRO_DONE_FORMAT = "Done assigning role {0}:{1} to all users";
    public const string MESSAGE_AUTOROLE_REMOVE_ALL_DONE_FORMAT = "Done removing role {0}:{1} from all users";
    #endregion
  }
}