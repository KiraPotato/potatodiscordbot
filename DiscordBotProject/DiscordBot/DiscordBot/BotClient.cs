﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */
using System;
using System.Threading.Tasks;

using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity;

using DiscordBot.Config;
using DiscordBot.Commands;
using DiscordBot.Config.GuildConfigs.CommandConfigs;
using DiscordBot.Twitch;

namespace DiscordBot
{
  class BotClient
  {
    private const string ERROR_INVALID_DATA = "Main config data is invalid.";
    private const string MESSAGE_LEFT_GUILD = "Left guild {0}:{1}\n";
    private const string MESSAGE_JOINED_GUILD = "Joined guild {0}:{1}\n";

    private DiscordClient _client;
    private CommandsNextModule _commands;

    public async Task RunBotAsync()
    {
      if (MainConfig.IsDataValid == false)
      {
        Console.Write(ERROR_INVALID_DATA);
        Environment.Exit(-1);
        return;
      }

      _client = new DiscordClient(MainConfig.DiscordConfiguration);
      _commands = _client.UseCommandsNext(MainConfig.CommandsNextConfiguration);

      _client.UseInteractivity(new InteractivityConfiguration());

      registerCommands();
      registerToEvents();

      await _client.ConnectAsync();

      GuildConfigs.FileSaveCycle(5000, _client).GetAwaiter().GetResult();
      TwitchLive.Start().GetAwaiter().GetResult();

      await Task.Delay(-1);
    }

    private void registerCommands()
    {
      _commands.RegisterCommands<BasicCommands>();
      _commands.RegisterCommands<ModListCommands>();
      _commands.RegisterCommands<AutoRoleCommands>();
    }

    private void registerToEvents()
    {
      _client.Ready += onReady;
      _client.GuildCreated += onGuildCreated;
      _client.GuildDeleted += onGuildDeleted;
      _client.GuildMemberAdded += onMemberJoinedGuild;
    }

    private Task onMemberJoinedGuild(GuildMemberAddEventArgs e)
    {
      Console.WriteLine("User Joined");

      var config = GuildConfigs.FetchConfigs<AutoRoleConfig>(e.Guild);

      if (config == null || config.IsEnabled == false)
        return Task.CompletedTask;

      config.AssignRole(e).GetAwaiter().GetResult();

      return Task.CompletedTask;
    }

    private Task onReady(ReadyEventArgs e)
    {
      foreach (var item in _client.Guilds)
      {
        GuildConfigs.LoadGuildConfigs(item.Value);
      }


      return Task.CompletedTask;
    }

    private Task onGuildDeleted(GuildDeleteEventArgs e)
    {
      Console.Write(MESSAGE_LEFT_GUILD, e.Guild.Name, e.Guild.Id);

      return Task.CompletedTask;
    }

    private Task onGuildCreated(GuildCreateEventArgs e)
    {
      Console.Write(MESSAGE_JOINED_GUILD, e.Guild.Name, e.Guild.Id);
      GuildConfigs.LoadGuildConfigs(e.Guild);
      return Task.CompletedTask;
    }
  }
}