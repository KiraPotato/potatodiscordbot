﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */
namespace DiscordBot
{
  class Program
  {
    static void Main(string[] args)
    {
      var bot = new BotClient();

      bot.RunBotAsync().GetAwaiter().GetResult();
    }
  }
}
