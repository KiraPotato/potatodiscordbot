﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */
using System;
using System.Linq;
using System.Threading.Tasks;

using DSharpPlus.Interactivity;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;

using DiscordBot.Utilities;
using DiscordBot.Config.GuildConfigs.CommandConfigs;
using DiscordBot.Commands.Shared;

namespace DiscordBot.Commands
{
  public class ModListCommands : CommandBase<ModListCommandConfig>
  {
    [Command(Constants.COMMAND_MODLIST_NAME), Description(Constants.COMMAND_MODLIST_DESCRIPTION)]
    public async Task Mods(CommandContext context, string gameName = "")
    {
      var config = fetchConfig(context.Guild);

      if (config == null)
        return;

      if (config.IsAllowedToUse(context.Member) == false)
      {
        await context.Channel.SendMessageAsync(Constants.NOT_ALLOWED);
        return;
      }

      if (config.IsEnabled == false)
      {
        await context.Channel.SendMessageAsync(Constants.MESSAGE_MODLIST_DISABLED);
        return;
      }

      if (string.IsNullOrEmpty(gameName) || config.FetchEntryMessage(gameName, out var entryMessage) == false)
      {
        await context.Channel.SendMessageAsync(generateModEntries(config));
        return;
      }

      await context.Channel.SendMessageAsync(entryMessage);
    }

    [RequirePermissions(DSharpPlus.Permissions.ManageGuild)]
    [Command(Constants.COMMAND_MODLIST_ADD_NAME)]
    public async Task SaveModItem(CommandContext context, params string[] names)
    {
      var config = fetchConfig(context.Guild);

      if (config == null || names.Length == 0)
        return;

      if (config.IsAllowedToEdit(context.Member) == false)
      {
        await context.Channel.SendMessageAsync(Constants.NOT_ALLOWED);
        return;
      }

      if (config.IsEnabled == false)
      {
        await context.Channel.SendMessageAsync(Constants.MESSAGE_MODLIST_DISABLED);
        return;
      }

      var combinedNames = CommandUtilities.CombineString(", ", names);

      await context.Channel.SendMessageAsync
        (string.Format(
          Constants.MESSAGE_MODLIST_REQUESTING_REPLY_FORMAT, combinedNames));

      var interactivity = context.Client.GetInteractivityModule();

      MessageContext msg =
        await interactivity.WaitForMessageAsync(
          (msg) => waitForCorrectUser(msg, context),
          TimeSpan.FromMinutes(Constants.COMMAND_MODLIST_TIMEOUT)).ConfigureAwait(false);

      if (msg != null)
      {

        foreach (var name in names)
        {
          config.AddEntry(name, msg.Message.Content);
        }

        await context.Channel.SendMessageAsync(
          string.Format(Constants.MESSAGE_MODLIST_SUCCESS_REPLY_FORMAT, combinedNames));

        await context.Channel.SendMessageAsync(msg.Message.Content);
      }
      else
      {
        await context.Channel.SendMessageAsync(Constants.MESSAGE_MODLIST_TIMEOUT);
      }
    }

    [RequirePermissions(DSharpPlus.Permissions.ManageGuild)]
    [Command(Constants.COMMAND_MODLIST_REMOVE_NAME)]
    public async Task RemoveModItem(CommandContext context, params string[] names)
    {
      var config = fetchConfig(context.Guild);

      if (config == null || names.Length == 0)
        return;

      if (config.IsAllowedToEdit(context.Member) == false)
      {
        await context.Channel.SendMessageAsync(Constants.NOT_ALLOWED);
        return;
      }

      if (config.IsEnabled == false)
      {
        await context.Channel.SendMessageAsync(Constants.MESSAGE_MODLIST_DISABLED);
        return;
      }

      foreach (var name in names)
        config.RemoveEntry(name);

      await context.Channel.SendMessageAsync(string.Format(
          Constants.MESSAGE_MODLIST_REMOVE_SUCCESS_REPLY_FORMAT,
          CommandUtilities.CombineString(", ", names)));
    }

    [RequireOwner]
    [Command(Constants.COMMAND_MODLIST_USE_WHITELIST_CHANGE)]
    public async Task UseCommandWhitelistEdit(CommandContext context, bool isAllowed)
    {
      var config = fetchConfig(context.Guild);

      if (config == null)
        return;

      await editWhitelist(context, isAllowed, config.ToggleUseAllowance);
    }

    [RequireOwner]
    [Command(Constants.COMMAND_MODLIST_EDIT_WHITELIST_CHANGE)]
    public async Task EditCommandWhitelistEdit(CommandContext context, bool isAllowed)
    {
      var config = fetchConfig(context.Guild);

      if (config == null)
        return;

      await editWhitelist(context, isAllowed, config.ToggleEditAllowance);
    }

    private string generateModEntries(ModListCommandConfig config)
    {
      if (config == null || config.AvailableNames.Count == 0)
        return Constants.MESSAGE_MODLIST_AVAILABLE_BLANK;

      return string.Format(
        Constants.MESSAGE_MODLIST_AVAILABLE_REPLY_FORMAT,
        CommandUtilities.CombineString(", ", config.AvailableNames.ToArray()));
    }
  }
}