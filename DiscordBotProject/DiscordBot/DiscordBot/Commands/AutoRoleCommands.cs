﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */
using System.Threading.Tasks;

using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;

using DiscordBot.Commands.Shared;
using DiscordBot.Config.GuildConfigs.CommandConfigs;
using DiscordBot.Config.GuildConfigs.Shared;

namespace DiscordBot.Commands
{
  public class TwitchLiveCommands : CommandBase<TwitchLiveConfig>
  {

    [RequirePermissions(DSharpPlus.Permissions.ManageGuild)]
    [Command(Constants.COMMAND_TWITCH_LIVE_SET_CHANNEL)]
    public async Task SetTwitchLiveNotificationChannel(CommandContext context)
    {

    }

    [RequirePermissions(DSharpPlus.Permissions.ManageGuild)]
    [Command(Constants.COMMAND_TWITCH_LIVE_ADD)]
    public async Task AddTwitchChannel(CommandContext context, params string[] channelNames)
    {
      var config = fetchConfig(context.Guild);

      if (config == null)
        return;

      if (channelNames.Length == 0)
        return;

      if (config.IsAllowedToEdit(context.Member) == false)
      {
        await context.Channel.SendMessageAsync(Constants.NOT_ALLOWED);
        return;
      }

      if (config.FollowChannels(channelNames))
      {

      }
      else
      {

      }
    }

    [RequirePermissions(DSharpPlus.Permissions.ManageGuild)]
    [Command(Constants.COMMAND_TWITCH_LIVE_REMOVE)]
    public async Task RemoveTwitchChannel(CommandContext context, string channelName)
    {

    }

    [RequireOwner]
    [Command(Constants.COMMAND_TWITCH_LIVE_EDIT_WHITELIST_CHANGE)]
    public async Task EditWhitelistToggle(CommandContext context, bool isAllowed)
    {
      var config = fetchConfig(context.Guild);

      if (config == null)
        return;

      await editWhitelist(context, isAllowed, config.ToggleEditAllowance);
    }
  }

  public class AutoRoleCommands : CommandBase<AutoRoleConfig>
  {
    [Command(Constants.COMMAND_AUTOROLE_NAME)]
    [RequireUserPermissions(DSharpPlus.Permissions.ManageRoles)]
    public async Task SetAutoRoleOnJoin(CommandContext context)
    {
      var config = fetchConfig(context.Guild);

      if (config == null)
        return;

      if (config.IsAllowedToEdit(context.Member) == false)
      {
        await context.Channel.SendMessageAsync(Constants.NOT_ALLOWED);
        return;
      }

      var mentionedRoles = context.Message.MentionedRoles;

      if (mentionedRoles.Count != 1)
      {
        await context.Channel.SendMessageAsync(Constants.MESSAGE_AUTOROLE_INCORRECT_ROLES_MENTIONED);
        return;
      }

      var role = mentionedRoles[0];

      config.SetRoleId(role.Id);
      config.IsEnabled = true;

      await context.Channel.SendMessageAsync(string.Format(
        Constants.MESSAGE_AUTOROLE_ASSIGN_CONFIRMATION_FORMAT, role.Name, role.Id));
    }

    [Command(Constants.COMMAND_AUTOROLE_DISABLE_NAME)]
    [RequireUserPermissions(DSharpPlus.Permissions.ManageRoles)]
    public async Task DisableAutoRole(CommandContext context)
    {
      var config = fetchConfig(context.Guild);

      if (config == null)
        return;

      if (config.IsAllowedToEdit(context.Member) == false)
      {
        await context.Channel.SendMessageAsync(Constants.NOT_ALLOWED);
        return;
      }

      config.SetRoleId(0);
      config.IsEnabled = false;
      await context.Channel.SendMessageAsync(Constants.MESSAGE_AUTOROLE_DISABLED_CONFIRMATION);
    }

    [RequireOwner]
    [Command(Constants.COMMAND_AUTOROLE_REMOVE_ROLE_FROM_ALL_NAME)]
    public async Task RemoveRoleFromAllUsers(CommandContext context)
    {
      var config = fetchConfig(context.Guild);

      if (config == null)
        return;

      var mentionedRoles = context.Message.MentionedRoles;

      if (mentionedRoles.Count != 1)
      {
        await context.Channel.SendMessageAsync(Constants.MESSAGE_AUTOROLE_INCORRECT_ROLES_MENTIONED);
        return;
      }

      var role = mentionedRoles[0];

      foreach (var item in context.Guild.Members)
      {
        await item.RevokeRoleAsync(role, Constants.COMMAND_AUTOROLE_NAME);
      }

      await context.Channel.SendMessageAsync(string.Format(
        Constants.MESSAGE_AUTOROLE_REMOVE_ALL_DONE_FORMAT, role.Name, role.Id));
    }

    [RequireOwner]
    [RequireUserPermissions(DSharpPlus.Permissions.ManageRoles)]
    public async Task RetroactivelyAssignAutoRole(CommandContext context)
    {
      var config = fetchConfig(context.Guild);

      if (config == null)
        return;

      if (config.IsEnabled == false)
      {
        await context.Channel.SendMessageAsync(Constants.MESSAGE_AUTOROLE_ASSIGN_ROLE_FIRST);
        return;
      }

      var role = context.Guild.GetRole(config.RoleId);

      if (role == null)
        return;

      foreach (var member in context.Guild.Members)
        await member.GrantRoleAsync(role, Constants.COMMAND_AUTOROLE_NAME);

      await context.Channel.SendMessageAsync(string.Format(
        Constants.MESSAGE_AUTOROLE_ASSIGN_RETRO_DONE_FORMAT, role.Name, role.Id));
    }

    [Command(Constants.COMMAND_AUTOROLE_RETRO_ADD_NAME)]
    [RequireOwner]
    public async Task EditWhitelistToggle(CommandContext context, bool isAllowed)
    {
      var config = fetchConfig(context.Guild);

      if (config == null)
        return;

      await editWhitelist(context, isAllowed, config.ToggleEditAllowance);
    }
  }
}