﻿/* 
 * Project - PotatoDiscordBot
 * Author - Potato
 * Author Discord - Potato#6073
 * Git - https://bitbucket.org/KiraPotato/potatodiscordbot
 * 
 * Code is completely free, do whatever you want with it as long as you leave a mention of the original author.
 * For any questions feel free to contact me on discord.
 * New versions can be downloaded from the git
 */
using System;
using System.Linq;
using System.Threading.Tasks;

using DSharpPlus.Entities;
using DSharpPlus.CommandsNext;
using DiscordBot.Config.GuildConfigs.Shared;

namespace DiscordBot.Commands.Shared
{
  public class CommandBase<T> where T : BaseCommandConfig
  {
    protected T fetchConfig(DiscordGuild guild)
      => GuildConfigs.FetchConfigs<T>(guild);

    protected async Task editWhitelist(
      CommandContext context,
      bool addToWhitelist,
      Action<bool, DiscordRole[]> editAction)
    {
      if (context == null)
        return;

      var mentionedRoles = context.Message.MentionedRoles;

      if (mentionedRoles.Count == 0)
      {
        await context.Channel.SendMessageAsync(Constants.INVALID_ROLES);
        return;
      }

      editAction?.Invoke(addToWhitelist, mentionedRoles.ToArray());

      await context.Channel.SendMessageAsync(Constants.OPERATION_COMPLETE);
    }

    protected bool waitForCorrectUser(DiscordMessage discordMessage, CommandContext originalContext)
    {
      if (discordMessage == null)
        return false;

      if (discordMessage.Channel != originalContext.Channel)
        return false;

      if (discordMessage.Author != originalContext.Message.Author)
        return false;

      return true;
    }
  }
}